//Boxes in different sizes
const PRODS = {
  2: {
    name: '2 Piece Box',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/Francium_custom_box_6.png?v=1659596390',
    shopify_id: 7932594094267,
    variant_id: 43200749928635,
    price: 120
  },
  6: {
    name: '6 Piece Box',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/Francium_custom_box_6.png?v=1659596390',
    shopify_id: 7606402252987,
    variant_id: 43200749928635,
    price: 120
  },
  12: {
    name: '12 Piece Box',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/Francium_custom_box_12.png?v=1659596390',
    shopify_id: 7606406971579,
    variant_id: 43200765952187,
    price: 240
  },
  24: {
    name: '24 Piece Box',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/Francium_custom_box_24.png?v=1659596390',
    shopify_id: 7606408249531,
    variant_id: 43200773652667,
    price: 480
  }
}
//sleeves
const S = {
  1: {
    name: 'Congrats',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/congrats_sleeve.png?v=1659642280'
  },
  2: {
    name: 'Get Well',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/Get_Well_Sleeve.png?v=1659642270'
  },
  3: {
    name: 'Anniversary',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/happy_anniversary_sleeve.png?v=1659650568'
  },
  4: {
    name: 'Birthday',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/happy_birthday_sleeve.png?v=1659650489'
  },
  5: {
    name: 'I am sorry',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/i_m_sorry_sleeve.png?v=1659653406'
  },
  6: {
    name: 'Francium',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/francium_sleeve.png?v=1659653833'
  },
  7: {
    name: 'Thank You',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/thank_you_sleeve.png?v=1659652064'
  }
}
//flavors
const C = {
  1: {
    name: 'Cocoa Nib Pistachio',
    dtl_heading: 'Cocoa Nib Pistachio',
    description: 'Pistachio & Cacao Nibs white chocolate bars have yummy roasted Pistachios and Cacao Nibs which are shelled out from the best Cacao Beans around the world after roasting the cocoa beans at the perfect temperature.',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/cocoa_nib_nougat.png?v=1660019184',
    tag: 'milk'
  },
  2: {
    name: 'Orange Pistachio',
    dtl_heading: 'Orange Pistachio',
    description: 'Caramel detail Caramel detail Caramel detail Caramel detail Caramel detail Caramel detail Caramel detail Caramel detail Caramel detail Caramel detail ',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/Orange_Pistachio.png?v=1660023145',
    tag: 'dark'
  },
  3: {
    name: 'White Cherry Ganache',
    dtl_heading: 'White Cherry Ganache',
    description: 'White & Dark detail White & Dark detail White & Dark detail White & Dark detail White & Dark detail White & Dark detail White & Dark detail White & Dark detail White & Dark detail White & Dark detail ',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/White_Cherry_Ganache.png?v=1660023451',
    tag: 'dark white'
  },
  4: {
    name: 'Matcha Hojicha',
    dtl_heading: 'Matcha Hojicha',
    description: 'Dark Choc detail Dark Choc detail Dark Choc detail Dark Choc detail Dark Choc detail Dark Choc detail Dark Choc detail Dark Choc detail Dark Choc detail Dark Choc detail ',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/matcha_hojicha.png?v=1660023759',
    tag: 'dark'
  },
  5: {
    name: 'Hazelnut Praliné',
    dtl_heading: 'Hazelnut Praliné',
    description: 'Flavor 5 detail Flavor 5 detail Flavor 5 detail Flavor 5 detail Flavor 5 detail Flavor 5 detail Flavor 5 detail Flavor 5 detail Flavor 5 detail Flavor 5 detail ',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/Hazelnut_Praline.png?v=1660024299',
    tag: 'milk'
  },
  6: {
    name: 'Caramelized Pecan',
    dtl_heading: 'Caramelized Pecan',
    description: 'Strawberry detail Strawberry detail Strawberry detail Strawberry detail Strawberry detail Strawberry detail Strawberry detail Strawberry detail Strawberry detail Strawberry detail ',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/caramelized_Pecan.png?v=1660024616',
    tag: 'milk'
  },
  7: {
    name: 'Banana Foster',
    dtl_heading: 'Banana Foster',
    description: 'Orange detail Orange detail Orange detail Orange detail Orange detail Orange detail Orange detail Orange detail Orange detail Orange detail ',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/Banana_Foster.png?v=1660024871',
    tag: 'white'
  },
  8: {
    name: 'Yuzu Lemon Caramel',
    dtl_heading: 'Passion fruit',
    description: 'Passion fruit detail Passion fruit detail Passion fruit detail Passion fruit detail Passion fruit detail Passion fruit detail Passion fruit detail Passion fruit detail Passion fruit detail Passion fruit detail ',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/Lemon_Caramel.png?v=1660025149',
    tag: 'none'
  },
  9: {
    name: 'Apple Pie',
    dtl_heading: 'Apple Pie',
    description: 'Raspberry detail Raspberry detail Raspberry detail Raspberry detail Raspberry detail Raspberry detail Raspberry detail Raspberry detail Raspberry detail Raspberry detail ',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/Apple_Pie.png?v=1660025488',
    tag: 'none'
  },
  10: {
    name: 'Madagascar Vanilla Milk Dark Fusion',
    dtl_heading: 'Madagascar Vanilla',
    description: 'Roast Coffee detail Roast Coffee detail Roast Coffee detail Roast Coffee detail Roast Coffee detail Roast Coffee detail Roast Coffee detail Roast Coffee detail Roast Coffee detail Roast Coffee detail ',
    img: 'https://cdn.shopify.com/s/files/1/0523/0051/3467/files/Madagascar_Vanilla.png?v=1660025760',
    tag: 'none'
  }
}
