/**
 * Include your custom JavaScript here.
 *
 * We also offer some hooks so you can plug your own logic. For instance, if you want to be notified when the variant
 * changes on product page, you can attach a listener to the document:
 *
 * document.addEventListener('variant:changed', function(event) {
 *   var variant = event.detail.variant; // Gives you access to the whole variant details
 * });
 *
 * You can also add a listener whenever a product is added to the cart:
 *
 * document.addEventListener('product:added', function(event) {
 *   var variant = event.detail.variant; // Get the variant that was added
 *   var quantity = event.detail.quantity; // Get the quantity that was added
 * });
 *
 * If you just want to force refresh the mini-cart without adding a specific product, you can trigger the event
 * "cart:refresh" in a similar way (in that case, passing the quantity is not necessary):
 *
 * document.documentElement.dispatchEvent(new CustomEvent('cart:refresh', {
 *   bubbles: true
 * }));
 */

$(document).ready(function (){
  if (window.innerWidth < 768) {
    $('#sm_grid_wrapper').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 1500,
    });

  }
  if (window.innerWidth < 1007) {
    $('#blogs').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 1500,
      dots: true,
      arrows: false
    });

  }
  $('.Collapsible--reviews button').trigger('click');

  /*  Video mute/unmute  */
  function mute_all(){
    $('video[class^="home-carousel__video-video"]').prop('muted', true)
    $('.bgvid_unmute_btn').show()
    $('.bgvid_unmute_btn_mob').show()
    $('.bgvid_unmute_btn_btm').css('opacity', 1)
  }

  $('.bgvid_unmute_btn').on('click', () => {
    mute_all()
    $('.home-carousel__video-video').prop('muted', false)
    $('.bgvid_unmute_btn').hide()
  })
  $('.home-carousel__video-video').on('click', () => {
    if ($('.home-carousel__video-video').prop('muted')) return //if muted, dont do anything
    $('.home-carousel__video-video').prop('muted', true)
    $('.bgvid_unmute_btn').show()
  })

  $('.bgvid_unmute_btn_mob').on('click', () => {
    mute_all()
    $('.home-carousel__video-video_mob').prop('muted', false)
    $('.bgvid_unmute_btn_mob').hide()
  })
  $('.home-carousel__video-video_mob').on('click', () => {
    if ($('.home-carousel__video-video_mob').prop('muted')) return //if muted, dont do anything
    $('.home-carousel__video-video_mob').prop('muted', true)
    $('.bgvid_unmute_btn_mob').show()
  })
  $('.bgvid_unmute_btn_btm').on('click', (e) => {
    e.preventDefault()
    //detect if button is hidden
    const is_hidden = ($('.bgvid_unmute_btn_btm').css('opacity') == '0')
    if (is_hidden) {//show unmute
      $('.home-carousel__video-video_btm').prop('muted', true);
      $('.home-carousel__video-video_btm').attr('muted', true)
      $('.bgvid_unmute_btn_btm').css('opacity', 1)
    } else {
      mute_all()
      $('.home-carousel__video-video_btm').prop('muted', false);
      $('.home-carousel__video-video_btm').attr('muted', false)
      $('.bgvid_unmute_btn_btm').css('opacity', 0)
    }
    // $('.bgvid_unmute_btn_btm').css('background-image', 'none')
  })
  $('.home-carousel__video-video_btm').on('click', (e) => {
    e.preventDefault()
    if ($('.home-carousel__video-video_btm').prop('muted')) return //if muted, dont do anything
    $('.home-carousel__video-video_btm').prop('muted', true);
    $('.home-carousel__video-video_btm').attr('muted', true)
    $('.bgvid_unmute_btn_btm').css('opacity', 1)
  })

  if (window.location.href.includes('customer_posted=true')) {
    document.getElementById("section-template--15251417923771__16392832667e0044fb").scrollIntoView();
  }

  $('.marquee__inner img').on('click', (e) => {
    const $i = $(e.target)
    const url = ($i.data('url'))
    if (url.startsWith('http')) window.open(url, '_blank')
  })


  /*if (detect_mob()) {
    // $('#bgvid_div_wrapper').append(`<div style="padding:177.78% 0 0 0;position:relative;"><iframe id="bgvid_iframe" src="https://player.vimeo.com/video/722430350?h=9b5a62279e&amp;autoplay=1&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="francium_slideshow_mobil"></iframe></div>`)
    const bgvid_iframe = document.getElementById('bgvid_iframe')
    window.vimeo_player = new Vimeo.Player('bgvid_div_wrapper_inner', {
      id: 722430350,//b3t 6/19
      autoplay: true,
      autopause: false,
      muted: true,//b3t 6/19
      background: true,
// 				byline: false,
//         		title: false,
      control: true,
      height: 'auto',
      width: '500px',
      loop: true
    });

    const device_width = window.innerWidth
    let width_calc = 1.28 * device_width + 188 //calculate
    if (device_width >= 360 && device_width <= 370) width_calc = 648
    if (device_width >= 414 && device_width <= 427) width_calc = 721
    if (device_width >= 428 && device_width <= 440) width_calc = 750
    const bgvid_div_wrapper_width_1 = `${width_calc - 1}px`
    const bgvid_div_wrapper_width = `${width_calc}px`
    setTimeout(() => {
      console.log(`fixing mobile iframe height bug`)
      $('#bgvid_div_wrapper').height(bgvid_div_wrapper_width_1)
      setTimeout(() => $('#bgvid_div_wrapper').height(bgvid_div_wrapper_width), 500)
    }, 500);//fix bug of mobile device calculates iframe height wrong, leaving a blank space at the top. Force browser to re-calculate height

  } else {
    window.vimeo_player = new Vimeo.Player('bgvid_div_wrapper_inner', {
      id: 722428332,//b3t 6/19
      autoplay: true,
      autopause: false,
      muted: true,//b3t 6/19
      background: false,
// 				byline: false,
//         		title: false,
      control: true,
      loop: true
    });
  }*/
  window.jq = $
  const device_width = window.innerWidth
  let width_calc = 1.28 * device_width + 188 //calculate
  if (device_width >= 360 && device_width <= 370) width_calc = 648
  if (device_width >= 414 && device_width <= 427) width_calc = 721
  if (device_width >= 428 && device_width <= 440) width_calc = 750
  const bgvid_div_wrapper_width_1 = `${width_calc - 1}px`
  const bgvid_div_wrapper_width = `${width_calc}px`
  setTimeout(() => {
    console.log(`fixing mobile iframe height bug`)
    $('#bgvid_div_wrapper').height(bgvid_div_wrapper_width_1)
    setTimeout(() => $('#bgvid_div_wrapper').height(bgvid_div_wrapper_width), 500)
  }, 500);//fix bug of mobile device calculates iframe height wrong, leaving a blank space at the top. Force browser to re-calculate height

  window.addEventListener('scroll', scroll_cb)
})

/**
 * Detect whether the element is within view. For example, is the video element fully in view?
 * @param allowance_top Extra top in pixel. For example, element can scroll off the top for an extra 50px and still be considered in view
 * @param allowance_bottom Extra bottom
 * @param ele Element. Default to the caller (this)
 * @returns {boolean}
 */
$.fn.isOnScreen = function (allowance_top = 0, allowance_bottom = allowance_top, ele){
  if (typeof this == "object" && (!ele || typeof ele !== "object")) ele = this
  const win = $(window);
  if (!ele[0] || (typeof ele[0] !== 'object')) return false
  const rect = ele[0].getBoundingClientRect()
  if (rect.top < (0 - allowance_top)) return false // top is over-the-top
  const vh = win.height()
  if ((rect.top + ele.height()) > (vh + allowance_bottom)) return false //bottom is under-the-bottom
  return true
}


scroll_cb = function (){
  if (typeof $.fn.isOnScreen !== "function") return
  if (window.innerWidth > 1621) return //not an ipad
  // if (! (/Macintosh/i.test(navigator.userAgent))) {
  //   return //not an ipad
  // }

  if ($('div.home_slider_btm').isOnScreen(100)) {
    toggle_video_btm(true)
  } else {
    toggle_video_btm(false)
  }
}

toggle_video_btm = function (is_play = true){
  const e = $('.home-carousel__video-video_btm')[0]
  if (!e || !(e.play) || !(e.pause)) return //element not a video
  if (is_play === true) e.play()
  if (is_play === false) e.pause()

}
