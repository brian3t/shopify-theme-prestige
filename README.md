curl -s https://shopify.dev/themekit.py | sudo python3

to get theme list: 
theme get --list --password=shptka_8dbfffd8e214373939a6cc54ab561058 --store="www-franciumchocolate-com.myshopify.com"

LIVE theme:
theme get --password=shptka_8dbfffd8e214373939a6cc54ab561058 --store="www-franciumchocolate-com.myshopify.com" --themeid=128171933883   
(or run ```tgl```)

#### To upload live theme:

theme --allow-live watch

#### To pull live theme:

```tgl```

